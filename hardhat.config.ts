import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.10",
  network: {
    hardhat:{
      chainId: 1337
    }
  }
};
